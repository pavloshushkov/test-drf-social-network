from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.decorators import detail_route

from api.models import User, Post, Like, Dislike
from api.serializers import UserSerializer, PostSerializer
from .permissions import IsOwnerOrReadOnly

# Create your views here.
class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    permission_classes = (IsAuthenticated, )

    def get_permissions(self):
        if self.action in ('update', 'destroy'):
            self.permission_classes = (IsOwnerOrReadOnly, )
        return super(self.__class__, self).get_permissions()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @detail_route()
    def like(self, request, pk=None):
        post = self.get_object()
        query_result = Like.objects.filter(user=request.user.id, post=post.id)

        if query_result:
            query_result[0].delete()
        else:
            row = Like(user=request.user, post=post)
            row.save()

        data = {
            'response': ['Liked']
        }
        return Response(data, status.HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action is 'create':
            self.permission_classes = (AllowAny, )
        elif self.action in ('update', 'destroy'):
            self.permission_classes = (IsOwnerOrReadOnly, )

        return super(self.__class__, self).get_permissions()