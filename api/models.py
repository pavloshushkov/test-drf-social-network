from django.db import models

from django.contrib.auth.models import AbstractUser
# Create your models here.


class User(AbstractUser):
    email = models.EmailField('email address',
                              unique=True,
                              help_text='Unique identifier for the user ')

class Post(models.Model):
    author = models.ForeignKey(User, related_name="posts")
    title = models.CharField(max_length=120)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('pk', )

    def likes(self):
        return Like.objects.filter(post=self.id).count()

    def dislikes(self):
        return Dislike.objects.filter(post=self.id).count()

class Like(models.Model):
    user = models.ForeignKey(User, related_name='user_likes')
    post = models.ForeignKey(Post, related_name='post_likes')


class Dislike(models.Model):
    user = models.ForeignKey(User, related_name='user_dislikes')
    post = models.ForeignKey(Post, related_name='post_dislikes')

