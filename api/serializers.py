from rest_framework import serializers
from api.models import User, Post




class PostSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Post
        fields = ('url', 'id', 'title', 'content', 'likes', 'dislikes', 'created_at', 'updated_at', 'author')

class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    posts = serializers.HyperlinkedRelatedField(many=True, view_name='post-detail', read_only=True)
    password = serializers.CharField(min_length=8, max_length=128, write_only=True)

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'first_name', 'last_name', 'posts', 'password')

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        super(UserSerializer, self).update(instance, validated_data)
        instance.set_password(validated_data['password'])
        instance.save()
        return instance