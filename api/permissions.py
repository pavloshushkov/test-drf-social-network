from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS or request.user.is_superuser and request.user.is_staff:
            return True

        if hasattr(obj, 'author'):
            return obj.author == request.user
        else:
            return obj == request.user