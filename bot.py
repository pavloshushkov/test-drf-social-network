import json
import requests
from random import sample, randint
from mimesis import Person, Text




URL_API = 'http://127.0.0.1:8000/api/v1'


def load_bot_config(file):
    load_json = open(file)
    values = json.load(load_json)
    number_of_users = values.get('number_of_users')
    max_posts_per_user = values.get('max_posts_per_user')
    max_likes_per_user = values.get('max_likes_per_user')

    return (number_of_users, max_posts_per_user, max_likes_per_user)

def requests_list_posts(user):
    headers = {'Authorization': 'JWT {}'.format(user['token'])}
    response = requests.get('http://127.0.0.1:8000/api/v1/posts/', headers = headers)
    # request to api
    # json deserialize response
    # return list posts
    return (list(filter(None, map(lambda x: x.get('id'), response.json()))))

def like_post_id(user, id_post):
    headers = {'Authorization': 'JWT {}'.format(user['token'])}
    response = requests.get('http://127.0.0.1:8000/api/v1/posts/{}/like'.format(int(id_post)),
                            headers=headers)

def magic(number_of_users, max_posts_per_user, max_likes_per_user):
    for i in range(number_of_users):
        person = Person('en')

        # create user
        user = {
            'username': person.name(gender=None),
            'password': person.password(length=8, hashed=False),
            'email': person.email(domains=None),
        }
        response = requests.post(URL_API + '/users/', data=user)
        print (user['username'], user['password'], user['email'])

        #auth user
        user_login = {
            'username': user.get('username'),
            'password': user.get('password')
        }
        response = requests.post('http://127.0.0.1:8000/api-token-auth/', data = user_login)
        print (response.json())
        user['token'] = response.json().get("token")

        # posting
        for b in range(max_posts_per_user + 1):
            text = Text()
            pre_load = {
                'title' : text.word(),
                'content' : text.title(),
            }
            headers = {'Authorization': 'JWT {}'.format(user['token'])}
            response = requests.post('http://127.0.0.1:8000/api/v1/posts/', data = pre_load,
                                     headers=headers)

        # likes random posts
        response_list_posts = requests_list_posts(user)
        for co in sample(range(len(response_list_posts)), randint(1, max_likes_per_user)):
            like_post_id(user, response_list_posts[co])



# print (load_bot_config('bot_config.json'))
print (magic(*load_bot_config('bot_config.json')))